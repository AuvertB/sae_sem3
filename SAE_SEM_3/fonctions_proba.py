import numpy as np

def trapeze(f, a, b, n):
    h = (b-a)/n
    x = a
    result = 0
    for i in range(1, n):
        x += h
        result += f(x)
    result = (f(a) + 2*result + f(b))*h/2
    return result

def simpson(f, a, b, n):
    h = (b-a)/n
    x = a
    result = f(x)
    for i in range(1, n):
        x += h
        if i % 2 == 0:
            result += 2*f(x)
        else:
            result += 4*f(x)
    result += f(b)
    result *= h/3
    return result

def rectangle(f, a, b, n):
    x = np.linspace(a, b, n+1)
    S = (b-a)/n * np.sum(f(x[0:n]))
    return S


def f1():
